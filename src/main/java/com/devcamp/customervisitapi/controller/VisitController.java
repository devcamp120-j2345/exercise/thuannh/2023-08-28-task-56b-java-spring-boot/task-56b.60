package com.devcamp.customervisitapi.controller;

import java.util.ArrayList;
import java.util.Date;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.customervisitapi.models.Customer;
import com.devcamp.customervisitapi.models.Visit;

@RestController
@RequestMapping
@CrossOrigin
public class VisitController {
    @GetMapping("/visits")
    public ArrayList<Visit> getVisitsApi(){
        //task 4
        Customer customer1 = new Customer("Linh");
        Customer customer2 = new Customer("Lan");
        Customer customer3 = new Customer("Trang");
        System.out.println(customer1);
        System.out.println(customer2);
        System.out.println(customer3);
        //task 5
        Visit visit1 = new Visit(customer1.getName(), new Date());
        Visit visit2 = new Visit(customer2.getName(), new Date());
        Visit visit3 = new Visit(customer3.getName(), new Date());
        System.out.println(visit1);
        System.out.println(visit2);
        System.out.println(visit3);
        //task 6
        ArrayList<Visit> visitList = new ArrayList<>();
        visitList.add(visit1);
        visitList.add(visit2);
        visitList.add(visit3);
        return visitList;
    }
    
}
